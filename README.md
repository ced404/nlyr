# nlyr

Simple, plain text song lyrics in terminal.

Lyrics from the [https://apiseeds.com/documentation/lyrics](https://apiseeds.com/documentation/lyrics) Lyrics API.

![nlyr](https://bitbucket.org/ced404/nlyr/raw/f2fbc6a3cb9d63f79358eb3dc2237f3e5ea90a1d/nlyr.png)

## Setup

- Add your API key to `~/.profile` or `~/.bash_profile`

`export NLYR_KEY=[key]`

- Install node dependencies (commander)

```shell
npm install
```

- make the `nlyr` (same as index.js) file executable.

```shell
chmod +x nlyr
```

- export the `nlyrics` folder path to your PATH environment.

## Usage

```shell
$ nlyr radiohead "fitter happier"
♩ Loading radiohead’s “fitter happier” lyrics…

Fitter, happier, more productive
comfortable
not drinking too much
regular exercise at the gym
(3 days a week)
…
```

https://bitbucket.org/ced404/nlyr/src/master/

## Notes

- https://developer.atlassian.com/blog/2015/11/scripting-with-node/
